import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from grabar import grabar, traer_audio_datos
from scipy import fft, arange
import numpy as np

def graficar():
    rng = traer_audio_datos()
    n = len(rng)
    k = arange(n)
    T = n / 44100
    frq = k / T
    frq = frq[range(int(n))]
    Y = fft(rng)
    Y = Y[range(int(n))]
    mY=abs(Y)
    locY = np.argmax(mY)
    print(frq[locY])
    frecuencia = frq[locY]
    animal = ""
    if frecuencia > 1150 and frecuencia < 1300:
        # 1214.4626524390244
        animal = "perrito"
        print('perrito')
    elif frecuencia > 800 and frecuencia < 1000:
        # 861
        animal = "gatico"
        print('gatico')
    elif frecuencia > 1600 and frecuencia < 1800:
        # 1717
        animal = "caballo"
        print('caballo')
    elif frecuencia > 2700 and frecuencia < 3300:
        # 2912
        animal = "cabra"
        print('cabra')
    elif frecuencia > 5000 and frecuencia < 5900:
        # 5778.601371951219 - 5640.748856707317
        animal = "chicharra"
        print('chicharra')
    elif frecuencia > 1400 and frecuencia < 1470:
        # 1409.7370426829268
        animal = "gallo"
        print('gallo')
    elif frecuencia > 200 and frecuencia < 800:
        # 484.5846036585366
        animal = "ballena"
        print('ballena')
    else:
        animal = "No concluyente, repita la prueba de nuevo"
        print('No concluyente, repita la prueba de nuevo')
    plt.plot(frq,  mY)
    plt.xlim(0,22000)
    plt.ylabel('Amplitud')
    plt.xlabel('Frecuencia')
    plt.title(animal)
    plt.show()

def main():
    grabar()
    graficar()

if __name__ == "__main__":
    main()
