import pyaudio
import wave
import numpy as np
import scipy.io.wavfile as waves


def grabar():
    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    RATE = 44100
    chunk = 256
    RECORD_SECONDS = 5
    WAVE_OUTPUT_FILENAME = "GRABACION.wav"
    p = pyaudio.PyAudio()
    for i in range(p.get_device_count()):
        pass
        # print(p.get_device_info_by_index(i))

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=chunk,
                    input_device_index=2
                    )

    print("Grabando ...")
    all = []
    for i in range(0, int(RATE / chunk * RECORD_SECONDS)):
        data = stream.read(chunk)
        all.append(data)
    print("Fin de la grabacion")
    stream.stop_stream()
    stream.close()
    p.terminate()

    # write data to WAVE file
    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(all))
    wf.close()

def traer_audio_datos():
    # print(termina,inicio,cont)
    archivo = 'GRABACION.wav'
    # muestreo: int muestras por segundo en el audio
    # sonido: array vector con todas las muestras del audio
    muestreo, sonido = waves.read(archivo,)
    # print(muestreo)
    # tamano: (int,) se utiliza con np.shape para
    # guardar todos los valores de los canales de
    # los audios en valores distintos
    tamano = np.shape(sonido)
    # muestras:int nos devuelve cuanto dura el primer canal del audio generalemente es la duracion del audio
    muestras = tamano[0]
    # print(muestras)
    # print(muestras)
    # m:int numero de canales que tiene el audio
    m = len(tamano)
    # duracion: float duracion del audio
    duracion = muestras / muestreo
    # canales monofinicos
    canales = 1
    # print("aqui")
    # si tiene mas de un canal (estereo o mas)
    if (m > 1):
        canales = tamano[1]
    # experimento con un canal
    if (canales > 1):
        canal = 0
        datos = sonido[:, canal]
    else:
        datos = sonido
    return datos